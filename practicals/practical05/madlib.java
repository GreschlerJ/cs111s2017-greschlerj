
/* ******************************************************************
The work I am submitting is a result of my own thinking and efforts.
Joshua Greschler
CMPSC 111 Spring 2017
Practical 05
Date: 02/24/17

Pourpse: to create a mad lib type game
********************************************************************* */

//imports scanner and date classes
import java.util.Scanner;
import java.util.Date;

	public class madlib
	{
	public static void main(String[] args)
{

	//declares all the variables used in the program
	String food, noun;
	int num;
	float price, netprice;

	System.out.println("Josh Greschler\nPractical 05\n" + new Date() + "\n");

	//asks user for inputs for story
	//food
	Scanner scan = new Scanner(System.in);
	System.out.println("Enter a food: ");
	food = scan.nextLine();
	
	//noun
	System.out.println("Enter a noun: ");
	noun = scan.nextLine();

	//number
	System.out.println("Enter a number: ");
	num = scan.nextInt();

	//price
	System.out.println("Enter a price: ");
	price = scan.nextFloat();

	//calculates net price based on the product
	netprice = (price * num);
	
	//tells a story
	System.out.println("\nWhen you give a college student a " + food);
	System.out.println("They of course will want a " + noun);
	System.out.println("The college students heard you have free food");
	System.out.println(num + " of them showed up to office hours");
	System.out.println("Now youre on the hook for $" + netprice);
}
}
