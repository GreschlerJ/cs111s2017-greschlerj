import java.lang.Math;

public class Kinetic {

  public static String computeVelocity(int kinetic, int mass) {
   //replaced "int" declarations with floats. worked, readout is in decimal form.
   //replacing ints in the computeVelocity line with floats does not allow for decimal inputs for K and m.
    float velocity_squared = 0;
    float velocity = 0;
    StringBuffer final_velocity = new StringBuffer();

    //System.out.println("F V: " + velocity);
    //System.out.println("F V squared: " + velocity_squared);

    if( mass != 0 ) {
      velocity_squared = 2 * (kinetic / mass);
     //Changed velocity squared from 3 to 2
      velocity = (float)Math.sqrt(velocity_squared);
      final_velocity.append(velocity);
    }

    else {
     final_velocity.append("You broke the laws of physics, you dope.");
    }

    //System.out.println("L V: " + velocity);
    //System.out.println("L V squared: " + velocity_squared);

    // why on earth is a comment structured like a line of code here?
    // I feel like these shouldnt be comments but Im afraid to delete them

    return final_velocity.toString();
  }

  public static void main(String[] args) {
    
    System.out.println("Bleep bloop...");
   //got rid of the cheesy calling line
    String velocity = computeVelocity(7,0);
    System.out.println("The velocity is: " + velocity);

}
 



}
