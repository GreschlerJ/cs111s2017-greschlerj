//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear()
    {
        boolean leap;
	leap = true;
	if ((year % 4) == 0) {
	System.out.println(year + " is a leap year");
	}


	return leap;
}
    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()
    {
	boolean cicada;
	cicada = true;
	if ((year % 17) == 0) {
	System.out.println(year + " is a cicada year");

	}
	return cicada;
}

       

    

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()
    {
	boolean spots;
	spots = true;
	if ((year % 11) == 0) {
	System.out.println(year + " is a peak sunspot year");
	}

	return spots;
}
}

