//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        activities.isLeapYear();
	activities.isCicadaYear();
	activities.isSunspotYear();
	
	/*
	if (leap = true) {
	System.out.println(userInput + " is a leap year");
	}
	if (cicada = true) {
	System.out.println(userInput + " is a cicada year");
	}
	if (spots = true) {
	System.out.println(userInput + " is a peak sunspot year");
	}
	*/

        System.out.println("Thank you for using this program.");
    }
}
