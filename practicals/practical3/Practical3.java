
/* ******************************************************************
The work I am submitting is a result of my own thinking and efforts.
Joshua Greschler
CMPSC 111 Spring 2017
Lab 2
Date: 01/26/17

Pourpse: To *simply* calculate the total of a bill
********************************************************************* */

import java.util.Date;

	public class Lab3
	{
	public static void main(String[] args)
{

	System.out.println("Josh Greschler\nLab 3\n"+ "\n");
		
	string name;
	float price;
	int pcnt;
	float tip;
	float total;
	int guests;
	double ppg;


	Scanner scan = new Scanner (System.in);

	System.out.print("Please enter your name: ");
	name = scan.nextString();
	 
	System.out.println("Hello, " + name);
	System.out.print("Please enter the price of your meal: $");
	price = scan.nextInt();

	System.out.print("Please enter the desired percent tip between 0-100: ");
	pcnt = scan.nextInt();
	
	tip = (pcnt * price) / 100;
	total = price + tip;

	System.out.println("The tip comes to $" + tip);
	System.out.println("The total with tip comes to $" + total);

	System.out.println("How many ways will the bill be split today?");
	guests = scan.nextInt();

	ppg = total / guests;

	System.out.println("The price per guest is $" + ppg);
	System.out.println("Thank you for using this program today");
	
}
}
}
