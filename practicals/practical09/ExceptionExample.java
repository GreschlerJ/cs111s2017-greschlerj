// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;

    try {
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
    }
    catch (NullPointerException ex) {
      System.out.println("Null pointer");
    }
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
    // throwsExceptions(0, X, "hi");
    // throwsExceptions(10, X, "");
    // throwsExceptions(10, X, "bye");
    // throwsExceptions(10, X, null);
       throwsExceptions(0, X, null);
  }

}
