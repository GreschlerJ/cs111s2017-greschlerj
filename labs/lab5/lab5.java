/* ***************************************************************
The work I am submitting is the result of my own thinking and efforts.
Joshua Greschler working with Mikey Spurr
CMPSC 111 Spring 2017
Lab 5
2/22/17

Pourpse: to hide a 10 character word in a 20x20 grid of letters
********************************************************* */

//import the scanner class to read the inputs
import java.util.Scanner;


	public class lab5
	{
	public static void main(String[] args)
	{

//declare the data type string and the variables word, changes 1 and 2
	String word, change1, change2;

	Scanner scan = new Scanner (System.in);

//asks user for their word to input
	System.out.print("Word: ");
	word = scan.nextLine();

	System.out.println("The word is: " + word);

//changes the word to all upper case
	change1 = word.toUpperCase();
//cuts the word off after the first 10 characters
	change2 = change1.substring(0, 10);

//outputs the wordhide
	System.out.println("AHGBBEJUEJUFLVLQFEHP");
	System.out.println("VKFWEUIBYWEFOWEFYBOO");
	System.out.println("BNFEKUWEFBGWFYEBUIWL");
	System.out.println("LDHEAJFGIHJGSDHEHAKA");
	System.out.println("UASFHPIUEFGPIWEUGFPW");
	System.out.println("APEJGIANVIEAPVYWYLBC");
	System.out.println("HKLTHBERPMIEMAEIAOQE");

//hides the changed word in the string of characters
	System.out.println("BHT" + change2 + "HQWMCIP");
	System.out.println("PKLMIAWEQIAIELVMUAXW");
	System.out.println("SFOHEFUIPWEFYGERYLHW");
	System.out.println("IGYESDFBSDFVNSDFAUST");
	System.out.println("EUGPRUGHPWRUIEFBNPIO");
	System.out.println("GFBWETFBIWEFTIWEFTYI");
	System.out.println("KEFGBWEFTDFDWEWEFQIO");
	System.out.println("CNDYWEGBWEWEFWFEWUIE");
	System.out.println("EWDGEWFWEFGWEFWEFHKO");
	System.out.println("NBNFEGYWGBEFDWEFGYWE");
	System.out.println("QGWEDGWEFGWEFGWEFQWE");
	System.out.println("KAWDEUQWRUKDEFRGUIWE");
	System.out.println("AIUFEUILWEFPIOAIBWEF");





	}
	}
