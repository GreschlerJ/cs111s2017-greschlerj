//=================================================
//Joshua Greschler, CS111S2017
//Lab 4
//All work is mine unless otherwise cited
// 
// Pourpse is to draw a pretty picture of the sun
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page){
	final int width = 600;
	final int height = 400;
	
//	sets background to blue for sky
	page.setColor(Color.cyan);
	page.fillRect(0, 0, width, height);

//	outtermost concentric ring is orange, rays below
	page.setColor(Color.orange);
	page.fillOval((width/2)-60, (height/2)-60, 120, 120);
	page.drawLine(width/2, height/2, 500, 0);
	page.drawLine(width/2, height/2, 400, -200);
	page.drawLine(width/2, height/2, -400, -400);

//	next ring is red, with rays
	page.setColor(Color.red);
	page.fillOval((width/2)-55, (height/2)-55, 110, 110);
	page.drawLine(width/2, height/2, 500, -600);
	page.drawLine(width/2, height/2, -500, 600);
	page.drawLine(width/2, height/2, -300, 100);

//	last ring is yellow, rays below
	page.setColor(Color.yellow);
	page.fillOval((width/2)-50, (height/2)-50 , 100, 100);
	page.drawLine(width/2, height/2, 300, 0);
	page.drawLine(width/2, height/2, -300, 0);
	page.drawLine(width/2, height/2, 0, 200);

//	didnt like the look of a yellow sun, wanted it to be white instead
	page.setColor(Color.white);
	page.fillOval((width/2)-45, (height/2)-45, 90, 90);

  }
}
