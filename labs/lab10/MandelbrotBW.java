//imports class bufferedimage to represent the image in pixels
import java.awt.image.BufferedImage;
//imports class to create a new file by running the code
import java.io.File;
//imports class to allow the image to be an output
import javax.imageio.ImageIO;



public class MandelbrotBW {
    public static void main(String[] args) throws Exception {
        //declares size of image
	int width = 1920, height = 1080, max = 1000;
        
	//creates a new image of type RGB specified by 8 bit integers, with size width and height
	BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0x000000, white = 0xFFFFFF;

	//The two for loops evaluate each pixel by going 'up' each column then over each row
        for (int row = 0; row < height; row++) {
	for (int col = 0; col < width; col++) {
                
		//functions allow conditional loops to evaluate wether each pixel should be black or white
		//something about infinite prime numbers? 'i' dunno.
		double c_re = (col - width/2)*4.0/width;
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                int iterations = 0;
                while (x*x+y*y < 4 && iterations < max) {
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;
                    x = x_new;
                    iterations++;
                }
                //loop evaluates black/white pixels
		if (iterations < max) image.setRGB(col, row, white);
                else image.setRGB(col, row, black);
            }
        }
        //writes the image to a png file called 'mandelbrot-bw.png'
	ImageIO.write(image, "png", new File("mandelbrot-bw.png"));
    }
}
