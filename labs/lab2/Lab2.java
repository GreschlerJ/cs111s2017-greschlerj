
/* ******************************************************************
The work I am submitting is a result of my own thinking and efforts.
Joshua Greschler
CMPSC 111 Spring 2017
Lab 2
Date: 01/26/17

Pourpse: to create a template to use on future programs
********************************************************************* */

import java.util.Date;

	public class Lab2
	{
	public static void main(String[] args)
{
	//outputs name and date
	System.out.println("Josh Greschler\nLab 2\n" + new Date() + "\n");

	//Variables
	int population = 10000000;    //rough guess of population of chicago
	int households = 4;           //4 people per household
	int pianos = 10;              //1 piano per 10 houses
	int need = 1000;              //1 piano repairman per 1000 pianos
	int PianoRepMen;
	int PianosInChicago;          //need this for next part about cost
	PianoRepMen = population / (households * pianos * need);
	PianosInChicago = population / (households * pianos);

	
	int breakingfreq = 2;         //each piano needs repair twice a year
	int daysinyear = 365;          
	int PianosPerDay;
	PianosPerDay = (PianosInChicago * breakingfreq) / (daysinyear * PianoRepMen);
	int ChiMedInc = 59558;       //median income in chicago
	int lifechoices = 10000;    //piano repair is not a lucrative proffesson
	int cost;
	
	cost = (ChiMedInc - lifechoices) / (daysinyear * PianosPerDay);

	System.out.println("The number of piano repairmen in Chicago is " + PianoRepMen);
	System.out.println("Each repairman fixes " + PianosPerDay + " pianos per day on average");
	System.out.println("The cost of a piano repair is $" + cost);

}
}
